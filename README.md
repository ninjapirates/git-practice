Practice Repo
=============

If you want to play around with how using a remote repository on BitBucket looks and works, use this.

## What I'd like everyone to try to do

1. Clone the repository
2. Edit the file `visitor-log.txt`
3. Create a commit for your change
4. Push that change back to the repository

Then again, but slightly more advanced:

1. Create a branch based on the `develop` branch
2. Make some change. You can edit the visitor log again, or add a file, or anything you like really.
3. Commit the change
4. Repeat steps 2 and 3 as many times as you'd like
5. Push your branch to BitBucket
6. On BitBucket, create a Pull Request for your branch

If everyone does this, it should hopefully give us a way to demo some simple rebasing and resolving of merge conflicts.

## Other ideas for things to do with this

- Remove and then re-add the [remote][aremote] on your
- Test your [SSH settings][assh]
- [Fetch][afetch] and merge (or pull) commits created by other people
- Check out how [markdown][amarkdown] works
- Try out [rebasing!][arebase]

[aremote]: https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes
[assh]: https://confluence.atlassian.com/bitbucket/ssh-keys-935365775.html
[afetch]: https://git-scm.com/docs/git-fetch
[amarkdown]: https://bitbucket.org/tutorials/markdowndemo/overview
[arebase]: https://git-scm.com/book/en/v2/Git-Branching-Rebasing
